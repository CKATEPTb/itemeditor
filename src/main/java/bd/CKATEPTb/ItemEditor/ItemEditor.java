package bd.CKATEPTb.ItemEditor;

import net.minecraft.command.CommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(modid = "itemeditor")
public class ItemEditor {

	@SidedProxy(clientSide = "bd.CKATEPTb.ItemEditor.CommonProxy", serverSide = "bd.CKATEPTb.ItemEditor.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
	}
	
	@SideOnly(Side.SERVER)
	@EventHandler
	public void preServerInit(FMLPreInitializationEvent e) {
		CommandHandler ch = (CommandHandler) FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager();
		ch.registerCommand(new INameCMD());
		ch.registerCommand(new ILoreCMD());
		System.out.println("Создал команды IName и ILore.");
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}

}
