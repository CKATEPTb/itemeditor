package bd.CKATEPTb.ItemEditor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.util.Constants;

public class ILoreCMD extends CommandBase {

	@Override
	public String getName() {
		return "ilore";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/ilore <add/remove> <Lore/Line number>";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		EntityPlayer player = (EntityPlayer) sender;
		if (args.length < 2) {
			player.sendMessage(new TextComponentString("§4/ilore <add/rem> Lore"));
			return;
		}
		if (player.getHeldItemMainhand().isEmpty()) {
			player.sendMessage(new TextComponentString("§4В руке пусто."));
			return;
		}

		
		NBTTagCompound displayTag = player.getHeldItemMainhand().getOrCreateSubCompound("display");
		NBTTagList loresList = displayTag.getTagList("Lore", Constants.NBT.TAG_STRING);
		
		if (args[0].equalsIgnoreCase("add")) {
			StringBuilder name = new StringBuilder();
			for (int i = 1; i < args.length; i++) {
				name.append(args[i]);
				if (i != args.length - 1)
					name.append(" ");
			}
			loresList.appendTag(new NBTTagString(name.toString().replaceAll("&", "§")));
			displayTag.setTag("Lore", loresList);
			return;
		}
		else if (args[0].equalsIgnoreCase("rem")||args[0].equalsIgnoreCase("remove")) {
			Integer i = null;
			try {
				i = Integer.parseInt(args[1]);
			} catch (Throwable ex) {
				player.sendMessage(new TextComponentString("§4"+args[1]+" не содержит число."));
				return;
			}
			i--;
			if(i<0||i>=loresList.tagCount()) {
				player.sendMessage(new TextComponentString("§4Число не должно выходить за диапазон от 1 до "+loresList.tagCount()));
				return;
			}
			loresList.removeTag(i);
			displayTag.setTag("Lore", loresList);
			return;
		}
		
	}
}
