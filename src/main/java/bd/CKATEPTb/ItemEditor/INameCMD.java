package bd.CKATEPTb.ItemEditor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class INameCMD extends CommandBase {

	@Override
	public String getName() {
		return "iname";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/iname Name";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		EntityPlayer player = null;
		if (!(sender instanceof EntityPlayer)) {
			sender.sendMessage(new TextComponentString("§4Эта команда для игроков."));
			return;
		}
		player = (EntityPlayer) sender;
		if (player.getHeldItemMainhand().isEmpty()) {
			sender.sendMessage(new TextComponentString("§4В руке пусто."));
			return;
		}
		if (args.length < 1) {
			player.sendMessage(new TextComponentString("§4/iname Name"));
			return;
		}

		NBTTagCompound displayTag = player.getHeldItemMainhand().getOrCreateSubCompound("display");
		StringBuilder name = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			name.append(args[i]);
			if (i != args.length - 1)
				name.append(" ");
		}
		displayTag.setString("Name", name.toString().replaceAll("&", "§"));
	}

}
